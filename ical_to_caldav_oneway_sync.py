# one-way-sync of an ical calendar to a caldav calendar.
# this is a straigt forward, maybe dirty, usage of some python libraries.
# (some problems are solved by the way, (unique use case, funny file formats))
#
# Jonathan Kretschmer
# webmaster@esg-dresden.de
# Evangelische Studierendengemeinde Dresden, Webadmin
# 
# not sure which licence to use. 

import datetime
import urllib.request
import hashlib

# https://pypi.org/project/icalendar/ version 4.0.9 used
import icalendar
# License: BSD (https://github.com/collective/icalendar/blob/master/LICENSE.rst)

# https://pypi.org/project/caldav/ version 0.8.2 used
import caldav
# License: Apache Software License (https://github.com/python-caldav/caldav/blob/master/COPYING.APACHE) or GNU General Public License (GPL) (https://github.com/python-caldav/caldav/blob/master/COPYING.GPL)

ical_url = "https://example.org/ical.ics"

caldav_url = "https://example.org/caldav_endpoint/"
calendar_name = "caldav_calendar_name"
archive_calendar_name = "caldav_archive_calendar_name"
username = "username"
password = "password"


ical_string = urllib.request.urlopen(ical_url).read()
print("ical fetched")


# compare new version with last version of ics file,
# procede if there are changes
print("comparing versions")
ical_string_lines = ical_string.splitlines()
ical_string_lines_stripped = []
for line in ical_string_lines:
    if not (line.startswith(b'DTSTAMP:') and line.endswith(b'Z')):
        ical_string_lines_stripped.append(line)
file_recent_ical_hash = open('recent_ical_hash.txt', 'r')
recent_ical_hash = file_recent_ical_hash.read()
file_recent_ical_hash.close()
new_ical_hash = hashlib.md5(b'\n'.join(ical_string_lines_stripped)).hexdigest()
print(recent_ical_hash)
print(new_ical_hash)
if recent_ical_hash != "" and recent_ical_hash == new_ical_hash:
    exit()
else:
    file_recent_ical_hash = open('recent_ical_hash.txt', 'w')
    file_recent_ical_hash.write(new_ical_hash)
    file_recent_ical_hash.close()


print("archiving events, updating calendar")

# build up connection to caldav calendar
caldav_client = caldav.DAVClient(url=caldav_url, username=username, password=password)
my_principal = caldav_client.principal()
caldav_calendars = my_principal.calendars()
# choose correct calendar
my_caldav_calendar = None
my_caldav_archive_calendar = None
for caldav_calendar in caldav_calendars:
	if caldav_calendar.name == calendar_name:
		my_caldav_calendar = caldav_calendar
		break
	if calendar.name == archive_calendar_name:
		my_caldav_archive_calendar = calendar
if my_caldav_calendar == None or my_caldav_archive_calendar == None:
	print("calendar not found")
	exit()

# convert from bare ics file to icalenadar library format,
# to create an icalendar which just contains one event
calendar = icalendar.Calendar.from_ical(ical_string)
single_event_calendar_list = []
for i in range(1, len(calendar.subcomponents)):
	single_event_calendar = icalendar.Calendar()
	single_event_calendar.add_component(calendar.subcomponents[0])
	single_event_calendar.add_component(calendar.subcomponents[i])
	single_event_calendar_list.append(single_event_calendar)

# collect current uids from ical calendar and caldav calendar
my_caldav_calendar_events = my_caldav_calendar.events()
ical_uids = []
caldav_uids = []
for calendar in single_event_calendar_list:
	ical_uids.append(str(calendar.subcomponents[1]['UID']))
for event in my_caldav_calendar_events:
	caldav_uids.append(str(event.icalendar_instance.subcomponents[1]['UID']))

# determine which events do not exist in ical calendar anymore
deleted_event_uids = []
for uid in caldav_uids:
	if not uid in ical_uids:
		deleted_event_uids.append(uid)
print("to be archived:")
print(deleted_event_uids)


# archive events that where deleted from ical calendar
for uid in deleted_event_uids:
	event_to_archive = my_caldav_calendar.object_by_uid(uid)
	event_to_archive_ical = event_to_archive.icalendar_instance
	event_to_archive_ical.subcomponents[1]['DESCRIPTION'] = (
		event_to_archive_ical.subcomponents[1]['DESCRIPTION']
		+ "\n\n+++ archived at " + str(datetime.datetime.now()) + " +++")
	event_temp = my_caldav_archive_calendar.save_event(
		ical=event_to_archive_ical.to_ical())
	event_to_archive.delete()

# add and update remaining events
for calendar in single_event_calendar_list:
	event_temp = my_caldav_archive.save_event(ical=calendar.to_ical())

print("finished")
